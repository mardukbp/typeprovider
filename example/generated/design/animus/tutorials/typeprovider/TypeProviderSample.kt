package design.animus.tutorials.typeprovider

import kotlin.Int

data class User(
  val id: Int
)
